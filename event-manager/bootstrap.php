<?php
/*
Plugin Name: Event Manager
Plugin URI:
Description: Event Manager
Version:     1.0
Author:      Carl-Constantin Ebers
Author URI:
License: GPLv2
*/



if ( ! defined( 'ABSPATH' ) ) {
	die( 'Access denied.' );
}
require __DIR__ . '/vendor/autoload.php';


register_activation_hook( __FILE__, '\EventManager\CCEMEventManager::cc_em_activation');
$eventManager = new \EventManager\CCEMEventManager();
