<?php

namespace Config;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class DatabaseEntityManager
{
        /**
         * @var Singleton The reference to *Singleton* instance of this class
         */
        private static $instance;

        /**
         * Returns the *Singleton* instance of this class.
         *
         * @return Singleton The *Singleton* instance.
         */
        public static function getInstance()
        {
                if (null === static::$instance) {

                        $dir = __DIR__ . '/yaml';
                        $paths = array($dir);
                        $isDevMode = true;

                        $dbParams = array(
                            'driver'   => 'pdo_mysql',
                            'user'     => DB_USER,
                            'password' => DB_PASSWORD,
                            'dbname'   => DB_NAME,

                            'host'  =>    DB_HOST,
                                //'charset' => 'utf8mb4',
                                //'default_table_options' => array(
                                //    'charset'  => 'utf8mb4',
                                //    'collate'  => 'utf8mb4_unicode_ci'
                                //)
                        );


                        $config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);
                        $entityManager = EntityManager::create($dbParams, $config);

                        //static::$instance = new static();
                        static::$instance = $entityManager;
                }

                return static::$instance;
        }

        /**
         * Protected constructor to prevent creating a new instance of the
         * *Singleton* via the `new` operator from outside of this class.
         */
        protected function __construct()
        {


        }

        /**
         * Private clone method to prevent cloning of the instance of the
         * *Singleton* instance.
         *
         * @return void
         */
        private function __clone()
        {
        }

        /**
         * Private unserialize method to prevent unserializing of the *Singleton*
         * instance.
         *
         * @return void
         */
        private function __wakeup()
        {
        }
}
