<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;


//require_once( realpath( dirname( __FILE__ ) ).'/../../../config/db.php' );
$entityManager = \Config\DatabaseEntityManager::getInstance();
return ConsoleRunner::createHelperSet($entityManager);
