<?php
//init hooks, front controller for queries

namespace EventManager;

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
use EventManager\controller\DeRegisterEventController;
use EventManager\controller\EventsAdminPageController;
use EventManager\controller\EventsController;
use EventManager\controller\RegisterEventController;
use EventManager\model\EventsModel;
use EventManager\view\EventsAdminPageView;
use EventManager\view\EventsView;

//front controller
class CCEMEventManager  {

    private $eventModel;

    function __construct() {
        $this->register_hook_callbacks();
    }


    private function register_hook_callbacks()
    {

        //action
        add_action('init', array($this, 'init'));
        add_action( 'template_redirect', array( $this,'cc_em_template_redirect') );
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action( 'admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
        add_action('wp_login', array($this, 'login'));
        //filter
        add_filter( 'query_vars', array( $this,'cc_em_add_query_var') );
        register_activation_hook( __FILE__, array($this, 'cc_em_activation') );

    }

    public function enqueue_styles() {

        $style_url = plugins_url('/view/assets/dist/css/event-manager.css', __FILE__);
        wp_enqueue_style( 'events-manager-style', $style_url  );
    }


    public function admin_enqueue_scripts($hook) {

        // Load only on ?page=toplevel_page_events_admin
        if($hook != ('toplevel_page_events_admin' || 'toplevel_page_events_admin_sub_events') ) {
            return;
        }

        $plugins_url = plugins_url('/view/app/dist/js/main.js', __FILE__);

        wp_enqueue_script( 'events-admin-script', $plugins_url, array(), '1', true );
    }

    public function init()
    {
        if ( is_user_logged_in() ) {
            CCEMEventManager::cc_em_set_custom_rewrite_rules();

            //register cpt Event, front controller for queries, create shortcodes for view
            $this->eventModel = new EventsModel();
            $eventController = new EventsController($this->eventModel);
            $eventView = new EventsView($this->eventModel);
            //$this->wp_ajax_getUsers();
            //backend administration
            $optionPageController = new EventsAdminPageController($this->eventModel);
            $optionPageView = new EventsAdminPageView($this->eventModel);
            add_action( 'wp_ajax_getEvents', array($this,'wp_ajax_getEvents') );
            add_action( 'wp_ajax_addParticipant', array($this,'wp_ajax_addParticipant') );
            add_action( 'wp_ajax_removeParticipant', array($this,'wp_ajax_removeParticipant') );
            add_action( 'wp_ajax_getUsers', array($this,'wp_ajax_getUsers') );




        } else if( $GLOBALS['pagenow'] !== 'wp-login.php' ) {
            wp_redirect(home_url('/wp-login.php'));
        }



    }

    public function login() {
        wp_redirect(home_url("/"));
        exit;
    }

    public function wp_ajax_getEvents() {

        $this->eventModel = new EventsModel();
        $eventView = new EventsView($this->eventModel);
        $eventView->renderAjaxEvents();
    }

    public function wp_ajax_addParticipant() {

        $userID = intval($_GET["userID"]);
        $eventID = intval($_GET["eventID"]);

        $registerEventController = new RegisterEventController($this->eventModel);
        $registerEventController->registerUser($userID,$eventID);

        wp_die(); // this is required to terminate immediately and return a proper response

    }

    public function wp_ajax_removeParticipant() {

        $userID = intval($_GET["userID"]);
        $eventID = intval($_GET["eventID"]);

        $deRegisterEventController = new DeRegisterEventController($this->eventModel);
        $deRegisterEventController->deRegisterUser($userID,$eventID);

        wp_die(); // this is required to terminate immediately and return a proper response

    }

    public function wp_ajax_getUsers() {
        $this->eventModel = new EventsModel();
        $eventView = new EventsView($this->eventModel);
        $eventView->renderAjaxUsers();
    }

    public function cc_em_template_redirect() {
        //match get and post requests
        if ( 'register' === get_query_var( 'action' ) ) {

            $userID = get_query_var( 'user_id' );
            $eventID = get_query_var( 'event_id' );
            $page = get_query_var( 'action_page' );
            $registerEventController = new RegisterEventController($this->eventModel);

            var_dump($userID);
            var_dump($eventID);
            var_dump($page);

            //admin page
            if( !empty($page) && ( 'admin' == $page) && current_user_can('manage_options') )
            {
                var_dump('admin');
                $registerEventController->registerUser($userID,$eventID);
            }
            //regular user adding/removing its event
            else if( get_current_user_id() == $userID )
            {
                var_dump('regular user');
                $registerEventController->registerUser($userID,$eventID);
            }
            else
            {
                var_dump('not admin or authorised action');
            }


            wp_redirect( wp_get_referer() );
            exit;

        }
        else if ( 'de-register' === get_query_var( 'action' ) ) {

            $userID = get_query_var( 'user_id' );
            $eventID = get_query_var( 'event_id' );
            $page = get_query_var( 'action_page' );
            $deRegisterEventController = new DeRegisterEventController($this->eventModel);

            var_dump('de-register');
            var_dump($userID);
            var_dump($eventID);
            var_dump($page);

            //admin page
            if( !empty($page) && ( 'admin' == $page) && current_user_can('manage_options') )
            {
                var_dump('admin');
                $deRegisterEventController->deRegisterUser($userID,$eventID);
            }
            //regular user adding/removing its event
            else if( get_current_user_id() == $userID )
            {
                var_dump('regular user');
                $deRegisterEventController->deRegisterUser($userID,$eventID);
            }
            else
            {
                var_dump('not admin or authorised action');
            }


            wp_redirect( wp_get_referer() );
            exit;

        }
        else
        {
            return;
        }
    }

    public static function cc_em_set_custom_rewrite_rules()
    {
        //match get requests
        add_rewrite_rule( 'event-manager/admin/register/user/([0-9]+)/event/([0-9]+)/?',
            'index.php?action_page=admin&action=register&user_id=$matches[1]&event_id=$matches[2]',
            'top' );

        add_rewrite_rule( 'event-manager/admin/de-register/user/([0-9]+)/event/([0-9]+)/?',
            'index.php?action_page=admin&action=de-register&user_id=$matches[1]&event_id=$matches[2]',
            'top' );

        add_rewrite_rule( 'event-manager/admin/register/user/?',
            'index.php?action_page=admin&action=register',
            'top' );

        add_rewrite_rule( 'event-manager/admin/de-register/user/?',
            'index.php?action_page=admin&action=de-register',
            'top' );

        add_rewrite_rule( 'event-manager/register/user/([0-9]+)/event/([0-9]+)/?',
            'index.php?action=register&user_id=$matches[1]&event_id=$matches[2]',
            'top' );

        add_rewrite_rule( 'event-manager/de-register/user/([0-9]+)/event/([0-9]+)/?',
            'index.php?action=de-register&user_id=$matches[1]&event_id=$matches[2]',
            'top' );



    }

    public function cc_em_add_query_var($vars)
    {
        $vars[] = 'action';
        $vars[] = 'event_id';
        $vars[] = 'user_id';
        $vars[] = 'action_page';
        return $vars;
    }

    public static function cc_em_activation() {
        CCEMEventManager::cc_em_set_custom_rewrite_rules();
        flush_rewrite_rules();
        EventsModel::ccem_createCustomTables();

    }

}