<?php

namespace EventManager\controller;
use EventManager\model\EventsModel;

//controller

class DeRegisterEventController{

    private $model;
    private $userID;
    private $eventID;

    public function __construct(EventsModel $model) {

        $this->model = $model;
        //$this->userID = get_current_user_id();
        //$this->eventID = get_query_var( 'event-id' );
        $this->register_hook_callbacks();
    }

    private function register_hook_callbacks()
    {

        //action
        add_action('init', array($this, 'init'));
        //filter

    }

    public function init()
    {

    }

    public function deRegisterUser($userID,$eventID){
        $this->model->deRegisterUser($userID,$eventID);

    }

}
