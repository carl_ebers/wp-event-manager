var config       = require('../config')
if(!config.tasks.css) return

var gulp         = require('gulp')
var browserSync  = require('browser-sync')
var less         = require("gulp-less");
//var sourcemaps   = require('gulp-sourcemaps')
var LessAutoprefix = require('less-plugin-autoprefix');
//var autoprefixer = require('gulp-autoprefixer')
var path         = require('path')
var rename = require("gulp-rename")

var paths = {
    src: path.join(config.root.src, config.tasks.css.src),
    dest: path.join(config.root.dest, config.tasks.css.dest)
}

var cssTask = function () {

    console.log(paths.src);
    return gulp.src(paths.src)
        //.pipe(sourcemaps.init())
        .pipe(less({ plugins: [new LessAutoprefix(config.tasks.css.autoprefixer)] }))
        //.pipe(autoprefixer(config.tasks.css.autoprefixer))
        .pipe(rename("main.css"))
        .pipe(gulp.dest(paths.dest))
        //.pipe(browserSync.stream())
}

gulp.task('css', cssTask)
module.exports = cssTask