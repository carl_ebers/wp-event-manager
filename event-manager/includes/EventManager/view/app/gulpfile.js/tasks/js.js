var config       = require('../config')
if(!config.tasks.css) return

var gulp         = require('gulp')
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var path         = require('path')

var plumber = require('gulp-plumber');

var babelify = require('babelify');
var vueify = require('vueify');

var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');

var paths = {
    srcfile: path.join(config.root.src, config.tasks.js.src, config.tasks.js.srcfile),
    dest: path.join(config.root.dest, config.tasks.js.dest),
    destfile: config.tasks.js.destfile
};

var jsTask = function () {
    console.log('run js task');
    var bundler = browserify(paths.srcfile);

    bundler.transform(vueify);
    bundler.transform(babelify);

    return bundler.bundle()
        //.on('error', gutil.log.bind(gutil, 'Browserify Error'))

        .pipe(source(paths.destfile))

        .pipe(buffer())
        .pipe(plumber())
        //.pipe(uglify())
        .pipe(gulp.dest(paths.dest));
    //browserSync.reload();
};

gulp.task('js', jsTask);
module.exports = jsTask;