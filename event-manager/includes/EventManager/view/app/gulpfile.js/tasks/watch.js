var config       = require('../config')
if(!config.tasks.css) return
var gulp        = require('gulp');
var browserSync = require('browser-sync');

var path         = require('path')

var paths = {
    jssrc: path.join(config.root.src, config.tasks.js.src),
    jssrcfile: path.join(config.root.src, config.tasks.js.src, config.tasks.js.srcfile)
};

gulp.task('watch', function() {
    browserSync({
        open: false,
        server: {
            baseDir: './'
        }
    });

    gulp.watch(paths.jssrc +  "/**/*.js", ['js-watch']);
    gulp.watch(paths.jssrc +  "/**/*.vue", ['js-watch']);

});

gulp.task('js-watch', ['js'], function (done) {
    browserSync.reload();
    done();
});
