var $ = require('jquery');
require('bootstrap');

function setSameHeightOfElements(elements, callback) {

    //get elements and max height
    var maxHeight = 0;
    var elem;
    $.each(elements, function (key, value) {

        elem = $(value);
        removeInlineStyle(elem);
        if (elem.height() > maxHeight) {
            maxHeight = elem.height();
        }
    });

    //set elements to same height
    $.each(elements, function (key, value) {
        elem = $(value);
        elem.height(maxHeight);
    });

    //callback
    if (callback && typeof(callback) == "function") {
        callback();
    }


}

function removeInlineStyle(elements) {
    var elem;
    $.each(elements, function (key, value) {
        elem = $(value).removeAttr('style');
    });

}

function callme() {

    console.log('hello world!');

}
//parent-code
module.exports = {

    setSameHeightOfElements: function (elements) {

        var clientWidth = document.documentElement.clientWidth;

        if (clientWidth < 750) {
            removeInlineStyle($(elements));
        }
        else {
            window.setTimeout( setSameHeightOfElements, 25, $(elements), null

            );
        }

    }
//setSameHeightOfElements($(elements)),500


};

