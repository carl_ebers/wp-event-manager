var Vue = require('vue');

// This is the event hub we'll use in every
// component to communicate between them.
var eventHub = new Vue();
exports.eventHub = eventHub;