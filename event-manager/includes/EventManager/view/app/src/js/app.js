var Vue = require('vue');

var events = require('./components/events.vue');
var users = require('./components/users.vue');

// This is the event hub we'll use in every
// component to communicate between them.
//var eventHub = new Vue();
//exports.eventHub = eventHub;

var eventsAdminPageViewManageByName = new Vue({
    el: '#eventsAdminPageViewManageByName',
    render: function (createElement) {
        return createElement(events)
    },
    components : []
});

var eventsAdminPageViewManageByUser = new Vue({
    el: '#eventsAdminPageViewManageByUser',
    render: function (createElement) {
        return createElement(users)
    },
    components : []
});