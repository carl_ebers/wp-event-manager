<?php

namespace EventManager\view;

//view
use EventManager\model\EventsModel;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;

class EventsView  {

    private $model;
    private $output;
    private $twig;

    public function __construct(EventsModel $model) {

        $this->initTwig();
        $this->model = $model;
        $this->register_hook_callbacks();

        //add_filter( 'the_content', array( $this, 'cc_em_add_form_to_content' ) );
        //add_shortcode('allEvents', array($this,'allEventsFunction'));
        //add_shortcode('allEventsWithUserActions', array($this,'allEventsWithUserActionsFunction'));
        //add_shortcode('myAgendaEvents', array($this,'myAgendaEventsFunction'));
        add_shortcode('eventsMyAgenda', array($this,'eventsMyAgendaFunction'));

    }

    private function register_hook_callbacks()
    {

        //action
        add_action('init', array($this, 'init'));
        //filter

    }


    private function initTwig()
    {

        $path = realpath(__DIR__ . '/templates');
        $loader = new \Twig_Loader_Filesystem($path);
        $this->twig = new \Twig_Environment($loader, array(
            'debug' => true));
        $this->twig->addExtension(new \Twig_Extension_Debug());
    }

    public function init()
    {

    }

    function eventsMyAgendaFunction()
    {

        $allEvents = $this->model->getAllEvents();

        return ( $this->twig->render('eventsWithUserActions.html.twig', array(
                'allEvents' => $allEvents,
                'userID' => get_current_user_id()
            )
        ));

    }

    /*
    function allEventsFunction()
    {

        $allEvents = $this->model->getAllEvents();

        return ( $this->twig->render('allEvents.html.twig', array(
                'allEvents' => $allEvents
            )
        ));
    }


    function allEventsWithUserActionsFunction()
    {

        $allEvents = $this->model->getAllEvents();

        return ( $this->twig->render('eventsWithUserActions.html.twig', array(
                'allEvents' => $allEvents,
                'userID' => get_current_user_id()
            )
        ));
    }


    function myAgendaEventsFunction()
    {

        $allEvents = $this->model->getAllEvents();

        return ( $this->twig->render('allUserEvents.html.twig', array(
                'allEvents' => $allEvents,
                'userID' => get_current_user_id()
            )
        ));

    }
    */


    public function renderAjaxEvents() {

        $allEvents = $this->model->getAllEvents();
        $outputArray = array();
        foreach( $allEvents as $event)
        {
            $object = new \stdClass();

            $object->ID = $event->ID;
            $object->title = $event->title;
            $object->content = $event->content;
            $object->participants = $event->participants;
            $object->notParticipatingUsers = $event->notParticipatingUsers;
            $outputArray[] = $object;
        }

        wp_send_json( $outputArray );
    }

    public function renderAjaxUsers() {

        $allUsers = $this->model->getAllUsers();
        $outputArray = array();
        foreach( $allUsers as $user)
        {
            $object = new \stdClass();
            $object->id = $user->id;
            $object->userNicename = $user->userNicename;
            $object->events = $user->getEvents();
            $object->notParticipatingEvents = $user->getNotParticipatingEvents();
            $outputArray[] = $object;
        }

        wp_send_json( $outputArray );

    }

    public function render() {

        return $this->output;

    }

}
