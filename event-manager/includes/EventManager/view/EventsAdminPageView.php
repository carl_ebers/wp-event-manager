<?php

namespace EventManager\view;

//view
use EventManager\model\EventsModel;

class EventsAdminPageView  {

    private $model;
    private $output;
    private $twig;

    public function __construct(EventsModel $model) {

        $this->initTwig();
        $this->model = $model;
        $this->register_hook_callbacks();

    }

    private function register_hook_callbacks()
    {

        //action
        add_action('admin_menu', array($this, 'admin_menu'));
        add_action('init', array($this, 'init'));
        //filter

    }


    private function initTwig()
    {

        $path = realpath(__DIR__ . '/templates');
        $loader = new \Twig_Loader_Filesystem($path);
        $this->twig = new \Twig_Environment($loader);

    }

    public function init()
    {

    }

    public function admin_menu()
    {
        $this->addMenu();
    }

    private function addMenu()
    {
        add_menu_page( 'Events Admin', 'Events Admin',
            'manage_options', 'events_admin', array($this,'display_page_content'),
            '', 'bottom' );


        add_submenu_page( 'events_admin', 'Participants Admin', 'Participants Admin', 'manage_options',
            'events_admin_sub_events',  array($this,'display_submenu_events_participants_content') );


    }


    public function display_page_content() {

        echo("<div id='eventsAdminPageViewManageByName'></div>");
        //$allEvents = $this->model->getAllEvents();
        //echo( $this->twig->render('events-admin_page.html.twig', array(
        //        'allEvents' => $allEvents
        //    )
        //));
    }

    public function display_submenu_events_participants_content() {

        echo("<div id='eventsAdminPageViewManageByUser'></div>");
        //$allUsers = $this->model->getAllUsers();
        //echo( $this->twig->render('events-admin_page_participants.html.twig', array(
        //        'allUsers' => $allUsers
        //    )
        //));
    }


    public function render() {

        return $this->output;

    }

}
