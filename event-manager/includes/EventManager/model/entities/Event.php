<?php


namespace EventManager\model\entities;

/**
 * Event
 */
class Event
{

/*
 *
  public 'ID' => int 84
  public 'post_author' => string '1' (length=1)
  public 'post_date' => string '2016-09-20 15:54:18' (length=19)
  public 'post_date_gmt' => string '2016-09-20 14:54:18' (length=19)
  public 'post_content' => string '' (length=0)
  public 'post_title' => string 'Applications of Lasers in Bone and Dentistry' (length=44)
  public 'post_excerpt' => string '' (length=0)
  public 'post_status' => string 'publish' (length=7)
  public 'comment_status' => string 'closed' (length=6)
  public 'ping_status' => string 'closed' (length=6)
  public 'post_password' => string '' (length=0)
  public 'post_name' => string 'event-1' (length=7)
  public 'to_ping' => string '' (length=0)
  public 'pinged' => string '' (length=0)
  public 'post_modified' => string '2016-09-28 09:25:36' (length=19)
  public 'post_modified_gmt' => string '2016-09-28 08:25:36' (length=19)
  public 'post_content_filtered' => string '' (length=0)
  public 'post_parent' => int 0
  public 'guid' => string 'http://event-manager.app/?post_type=event&#038;p=84' (length=51)
  public 'menu_order' => int 0
  public 'post_type' => string 'event' (length=5)
  public 'post_mime_type' => string '' (length=0)
  public 'comment_count' => string '0' (length=1)
  public 'filter' => string 'raw' (length=3)
 *
 */



    public $ID;
    public $title;
    public $content;
    public $participants;
    public $notParticipatingUsers;


    public function __construct($id,$title,$content)
    {
        $this->ID = $id;
        $this->title = $title;
        $this->content = $content;
        $this->participants = array();
        $this->notParticipatingUsers = array();
    }

    public function getID()
    {
        return $this->ID;
    }


    public function setID($ID)
    {
        $this->ID = $ID;
    }

    public function getTitle()
    {
        return $this->title;
    }


    public function addParticipant(\WpUsers $participant)
    {
        $this->participants[] = $participant;
    }

    public function getParticipants()
    {
        return $this->participants;
    }

    public function addNotParticipatingUser(\WpUsers $user)
    {
        $this->notParticipatingUsers[] = $user;
    }

    public function getNotParticipatingUsers()
    {
        return $this->notParticipatingUsers;
    }


}

