<?php



/**
 * EventParticipant
 */
class EventParticipant
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $event_id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var string
     */
    private $registration_date;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventId
     *
     * @param integer $eventId
     *
     * @return EventParticipant
     */
    public function setEventId($eventId)
    {
        $this->event_id = $eventId;

        return $this;
    }

    /**
     * Get eventId
     *
     * @return integer
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return EventParticipant
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set registrationDate
     *
     * @param string $registrationDate
     *
     * @return EventParticipant
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registration_date = $registrationDate;

        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return string
     */
    public function getRegistrationDate()
    {
        return $this->registration_date;
    }
}

