<?php


//model
namespace EventManager\model;
use EventManager\model\entities\Event;

use Config\DatabaseEntityManager;

class EventsModel
{

    private $allEvents;
    private $allEventsWithUserActions;
    private $allEventsUserRegistered;
    private $allEventsParticipants;
    private $entityManager;


    public function __construct()
    {
        $this->entityManager = DatabaseEntityManager::getInstance();
        $this->register_hook_callbacks();

        $this->cc_em_register_post_types();

        $this->allEvents = array();
        $this->allEventsWithUserActions = array();
        $this->allEventsUserRegistered = array();
        $this->allEventsParticipants = array();

        $this->modelAllEvents();
    }

    private function register_hook_callbacks()
    {

        //action
        add_action('init', array($this, 'init'));
        //filter

    }

    public function isRegistered($userID,$eventID)
    {
        $eventParticipant = $this->entityManager->getRepository('EventParticipant')->findOneBy(array('event_id' => $eventID,'user_id' => $userID));
        if ($eventParticipant) {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function registerUser($userID,$eventID)
    {

        if( ! $this->isRegistered($userID,$eventID) )
        {
            var_dump('register user');
            $eventParticipant = new \EventParticipant();
            $eventParticipant->setUserId($userID);
            $eventParticipant->setEventId($eventID);
            $mysql_date_now = date("YmdHms");

            $eventParticipant->setRegistrationDate( $mysql_date_now );

            $this->entityManager->persist($eventParticipant);
            $this->entityManager->flush();
        }

    }

    public function deRegisterUser($userID,$eventID)
    {

        $eventParticipant = $this->entityManager->getRepository('EventParticipant')->findOneBy(array('event_id' => $eventID,'user_id' => $userID));
        if ($eventParticipant !== null) {
            $this->entityManager->remove($eventParticipant);
        }

        $this->entityManager->flush();

    }

    public function init()
    {

    }

    private function cc_em_register_post_types()
    {

        $labels = array(
            'name'               => _x( 'Events', 'post type general name', 'your-plugin-textdomain' ),
            'singular_name'      => _x( 'Event', 'post type singular name', 'your-plugin-textdomain' ),
            'menu_name'          => _x( 'Events', 'admin menu', 'your-plugin-textdomain' ),
            'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'your-plugin-textdomain' ),
            'add_new'            => _x( 'Add New', 'event', 'your-plugin-textdomain' ),
            'add_new_item'       => __( 'Add New Event', 'your-plugin-textdomain' ),
            'new_item'           => __( 'New Event', 'your-plugin-textdomain' ),
            'edit_item'          => __( 'Edit Event', 'your-plugin-textdomain' ),
            'view_item'          => __( 'View Event', 'your-plugin-textdomain' ),
            'all_items'          => __( 'All Event', 'your-plugin-textdomain' ),
            'search_items'       => __( 'Search Events', 'your-plugin-textdomain' ),
            'parent_item_colon'  => __( 'Parent Events:', 'your-plugin-textdomain' ),
            'not_found'          => __( 'No events found.', 'your-plugin-textdomain' ),
            'not_found_in_trash' => __( 'No events found in Trash.', 'your-plugin-textdomain' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Description.', 'your-plugin-textdomain' ),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'event' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor' )
        );

        register_post_type( 'event', $args );

    }

    public static function ccem_createCustomTables() {

        global $wpdb;
        global $charset_collate;

        //create table event_user
        $table_name = $wpdb->prefix . "ccem_event_user";
        $sql_create_table = "CREATE TABLE EventParticipants (id INT AUTO_INCREMENT NOT NULL, event_id INT NOT NULL, user_id INT NOT NULL, registration_date VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;";

        //create dummy events
        $args = array(
            'post_type' => 'event',
            'order' => 'ASC',
            'posts_per_page'=> '-1',
            'orderby' => 'title'
        );

        $wpLoop = new \WP_Query( $args );

        if( false === $wpLoop->have_posts() )
        {
            $my_post = array(
                'post_title'    => 'The secret messages of UK roads',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );


            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'The 10 best films of 2016',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'Why is qwerty on our keyboards?',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'The planet’s fastest commercial train',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'English has 3,000 words for being drunk',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'The planet’s fastest commercial train',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );

            //create dummy events
            $my_post = array(
                'post_title'    => 'How to learn an instrument in six weeks',
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type'     =>'event',
            );
            // Insert the post into the database.
            wp_insert_post( $my_post );
        }

        //update DB
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql_create_table );
    }

    private function modelAllEvents()
    {
        $args = array(
            'post_type' => 'event',
            'order' => 'ASC',
            'posts_per_page'=> '-1',
            'orderby' => 'title'
        );

        $wpLoop = new \WP_Query( $args );
        $wp_post_events  = $wpLoop->posts;
        //$wp_post_events  = array();

        //all users
        $allUsers = $this->entityManager->getRepository('WpUsers')->findAll();

        //create events objects and add participants to event objects
        $eventParticipants = $this->entityManager->getRepository('EventParticipant')->findAll();
        foreach($wp_post_events as $wp_post_event) {
            
            $event_obj = new Event($wp_post_event->ID,$wp_post_event->post_title,$wp_post_event->post_content);

            foreach($eventParticipants as $eventParticipant) {

                if(  $eventParticipant->getEventId() ==  $wp_post_event->ID )
                {
                    $user = $this->entityManager->getRepository('WpUsers')->findOneBy(array('id' => $eventParticipant->getUserId()  ));
                    //var_dump($user);
                    if(isset($user) && $user > 0 )
                    {
                        $event_obj->addParticipant($user);
                    }

                }
            }

            //add non participating users
            $tmp = $event_obj->getParticipants();
            foreach($allUsers as $user) {
                if( empty($tmp) || !in_array($user,$tmp) )
                {
                    $event_obj->addNotParticipatingUser($user);
                }
            }

            $this->allEvents[] = $event_obj;

        }

    }

    public function getAllEvents()
    {
        return $this->allEvents;
    }

    private function getSingleEvent($id)
    {
        $allEvents = $this->getAllEvents();
        $result = null;
        foreach($allEvents as $event) {
            if($id == $event->getId())
            {
                $result = $event;
                break;
            }
        }
        return $result;
    }

    public function getAllUsers()
    {

        //all users
        $allUsers = $this->entityManager->getRepository('WpUsers')->findAll();
        $eventParticipants = $this->entityManager->getRepository('EventParticipant')->findAll();
        $allEvents = $this->getAllEvents();

        foreach($allUsers as $user) {

            foreach($eventParticipants as $eventParticipant) {
                if(  $eventParticipant->getUserId() ==  $user->getId() )
                {
                    $user->addEvent( $this->getSingleEvent($eventParticipant->getEventId() ) );
                }

            }

            //add non participating users
            $tmp = $user->getEvents();
            foreach($allEvents as $event) {
                if( empty($tmp) || !in_array($event,$tmp) )
                {
                    $user->addNotParticipatingEvent($event);
                }
            }


            $this->allEventsParticipants[] = $user;
        }

        return $this->allEventsParticipants;
    }

    public function getAllEventsWithUserActions($userID)
    {
        //get post IDs
        $eventParticipants = $this->entityManager->getRepository('EventParticipant')->findBy(array('user_id' => $userID ));
        $participantEventIDs = array();
        foreach($eventParticipants as $eventParticipant) {
            $participantEventIDs[] = $eventParticipant->getEventId();
        }

        $allEvents = $this->getAllEvents();
        foreach($allEvents as $event) {

            if( in_array($event->getID(),$participantEventIDs) )
            {
                $event->userRegisteredToEvent = true;
            }
            $this->allEventsWithUserActions[] = $event;
            var_dump($event);

        }

        return $this->allEventsWithUserActions;
    }

    public function getAllUserEventsRegistered($userID)
    {
        //get post IDs
        $eventParticipants = $this->entityManager->getRepository('EventParticipant')->findBy(array('user_id' => $userID ));
        $participantEventIDs = array();
        foreach($eventParticipants as $eventParticipant) {
            $participantEventIDs[] = $eventParticipant->getEventId();
        }

        $allEvents = $this->getAllEvents();
        foreach($allEvents as $event) {
            if( in_array($event->ID,$participantEventIDs) )
            {
                $this->allEventsUserRegistered[] = $event;
            }

        }

        return $this->allEventsUserRegistered;

    }




}